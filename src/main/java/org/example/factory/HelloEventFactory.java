package org.example.factory;

import com.lmax.disruptor.EventFactory;
import org.example.model.MessageModel;

public class HelloEventFactory implements EventFactory<MessageModel> {
    @Override
    public MessageModel newInstance() {
        return new MessageModel();
    }
}