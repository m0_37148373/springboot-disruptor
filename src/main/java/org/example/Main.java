package org.example;

import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 简述
 *
 * @author brant
 * @since 2024-03-14
 */
@SpringBootApplication
public class Main {
    public static void main(String[] args) {
        System.out.println("Hello world!");
    }
}