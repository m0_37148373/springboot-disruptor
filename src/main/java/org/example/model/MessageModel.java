package org.example.model;

import lombok.Data;

/**
 * 消息体
 */
@Data
public class MessageModel {
    private String message;
}